CREATE DATABASE DBSuperMarket;

USE DBSuperMarket;
CREATE TABLE Products
(
	ProdID		int				PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	ProdName	varchar(300)	NOT NULL,
	Article		char(20)		NOT NULL UNIQUE CHECK (Article LIKE '[A-Z][A-Z][0-9][0-9][0-9][0-9][0-9][0-9]'),--regex ХХ999999
	Color		varchar(25)		NULL,
	ProdDate	DATETIME		NOT NULL CHECK (ProdDate >= 2020/01/01),-->2020 
	Country		varchar(50)		NOT NULL,
	ProdPrice	decimal(10,2)	NOT NULL,
	Currency	char(3)			NOT NULL DEFAULT 'UAH'
);

CREATE TABLE Invoice
(
	InvID		int				PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	InvNum		varchar(25)		NOT NULL,
	InvDate		DATETIME		NOT NULL,
	Sum_nt		decimal(10,2)	NOT NULL,
	Tax			decimal(10,2)	NULL,
	Sum_wt		decimal(21,2)	NULL,
	CustomID	int				NOT NULL
);

ALTER TABLE Invoice 
ADD FOREIGN KEY (CustomID) REFERENCES Customers (CustomID);


CREATE TRIGGER InvoiceINSERT ON Invoice
AFTER INSERT
AS
	UPDATE Invoice
	Set Tax = Sum_nt * 0.2
	UPDATE Invoice
	Set Sum_wt = Sum_nt + Tax



CREATE TABLE InvoiceDetail
(
	InvDID		int					PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	InvID		int					REFERENCES Invoice (InvID),
	ProdID		int					REFERENCES Products (ProdID),
	Quant		decimal(10,2)		NOT NULL,
	Price		decimal(10,2)		NOT NULL,
	Sum			decimal(10,2)		NULL
);

CREATE TRIGGER InvoiceDetail_INSERT ON InvoiceDetail
AFTER INSERT
AS
	UPDATE InvoiceDetail
	Set Sum = Quant * Price

CREATE TABLE Customers
(
	CustomID		int				PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	CustomName		varchar(50)		NOT NULL,
	CustomSurname	varchar(50)		NOT NULL,
	CustomMiddle	varchar(50)		NOT NULL,
	BeginDate		smalldatetime	NOT NULL CHECK (BeginDate >= 2020/01/01),-->2020 
	CustomMail		varchar(100)	CHECK (CustomMail !=''),
	CustomPhone		varchar(20)		NOT NULL UNIQUE CHECK (CustomPhone LIKE '+[3][8][0][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),--regex +38(099) 999-99-99
	Birthday		smalldatetime	NULL,
	Town			varchar(50)		NULL
);

CREATE TABLE Discount
(
	DiscID		int				PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	CustomID	int				REFERENCES Customers (CustomID),
	DiscDate	datetime		NULL,
	DiscPercent int				NULL,
	DiscSumma	decimal(10,2)	NULL,
	InvID		int				REFERENCES Invoice (InvID) NULL
);

CREATE TABLE currency 
(
  name   VARCHAR(20)	NULL,
  code   VARCHAR(3)		NULL,
  symbol VARCHAR(5)		NULL
);