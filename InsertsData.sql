SET DATEFORMAT YMD;

INSERT INTO Products (ProdName, Article, Color, ProdDate, Country, ProdPrice, Currency)
values 
('Валерьянка', 'AS348935', 'Белый', '2020/10/08' , 'Германия', 10.2, 'USD'),
('Пятерчатка', 'AK688935', 'Красный', '2020/10/08' , 'Франция', 15.8, 'EUR'),
('Спрей', 'AS348745', 'Серый', '2020/04/08' , 'США', 80.2, 'GBP'),
('Футболка', 'FS362935', 'Зеленый', '2020/010/08', 'Украина', 454.2, 'JPY'),
('Платье', 'AS478935', 'Черный', '2020/06/14' , 'Германия', 700.2, 'CHF'),
('Кофта', 'MS356935', 'Фиолетовый', '2020/07/18' , 'Польша', 380.2, 'CNY'),
('Бантик', 'AS148937', 'Оранжевый', '2020/01/20' , 'Индия', 50.2, 'RUB')
INSERT INTO Products (ProdName, Article, Color, ProdDate, Country, ProdPrice)
values ('Таблетка 6шт. 2бл.', 'AM639800', 'Green', '2021/01/28', 'Украина', 500.99),
('Шампунь Акция 2шт.', 'AM663800', 'Green', '2021/03/28', 'Украина', 77.99),
('Крем 6шт в уп', 'AM639847', 'Green', '2021/02/26', 'Украина', 90.99)
INSERT INTO Products (ProdName, Article, Color, ProdDate, Country, ProdPrice)
values ('Игрушка "Корона"', 'AL639800', 'Бежевый', '2020/09/11', 'Украина', 580.99),
('Иголка Акция 2шт.', 'SS663800', 'Фиолетовый', '2020/09/11', 'Украина', 7.99),
('Вакцина 6шт в уп', 'SS639847', 'Красный', '2020/09/11', 'Украина', 15000.99)



INSERT INTO Invoice (InvNum, InvDate, Sum_nt, CustomID)
values 
(1001,'2020/01/15', 20.2, 1),
(1002,'2020/02/15', 50.2, 2),
(1003,'2020/03/15', 500.2, 3),
(1004,'2020/07/16', 6178.2, 4),
(1005,'2020/05/15', 10.2, 5),
(1006,'2020/06/15', 785.2, 6),
(1007,'2020/09/11', 647.2, 7),
(1008,'2020/07/16', 200.2, 8),
(1009,'2020/09/15', 540.2, 9),
(1010,'2020/10/15', 80.2, 10),
(1011,'2020/11/15', 200.2, 11),
(1012,'2020/12/15', 200.2, 11),
(1013,'2021/01/15', 80.2, 13),
(1014,'2021/02/15', 600.2, 14),
(1015,'2021/03/15', 700.2, 15),
(1016,'2021/03/16', 50.2, 16),
(1017,'2021/03/17', 64700.2, 17),
(1018,'2021/09/11', 670.7, 18),
(1019,'2021/09/30', 60.2, 19),
(1020,'2021/09/26', 10.9, 20),
(1021,'2021/09/09', 706.8, 21),
(1022,'2020/09/09', 70.8, 22),
(1023,'2020/09/30', 940.8, 23),
(1024,'2020/09/26', 650.8, 24),
(1025,'2020/09/09', 706.8, 25),
(1026,'2020/07/16', 70.8, 26),
(1027,'2020/07/16', 10.8, 27),
(1028,'2020/07/16', 660.8, 28),
(1029,'2020/07/16', 158.8, 29)


INSERT INTO InvoiceDetail (InvID, ProdID, Quant, Price)
values 
(1,1,50,20),
(2,2,100,650),
(3,3,20,500),
(4,4,30,300),
(5,5,10,150),
(6,6,70,150),
(7,7,25,350),
(8,1,40,15),
(9,2,40,405),
(10,10,5,999.99),
(11,11,35,999.99),
(12,9,65,999.99),
(13,8,75,999.99),
(14,12,25,999.99),
(15,10,50,999.99),
(16,11,3,999.99),
(17,12,2,999.99),
(18,13,1,999.99)



INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomMail, CustomPhone, Birthday, Town)
values 
('Vika', 'Koolosko', 'Vladimirovna', '2020/01/15', 'lalala@mail.ru', '+380930331810', '1990/01/01', 'Kiev'),
('Nina', 'Veselka', 'Olegovna', '2020/02/15', 'parampampam@mail.ru', '+380930331811', '1995/01/01', 'Dnepr'),
('Natasha', 'Karpovna', 'Olegovna', '2020/06/15', 'detka@mail.ru', '+380930331812', '1996/01/01', 'Odessa'),
('Русалка', 'Красная', 'Морская', '2021/02/01', 'kroshka@mail.ru', '+380930331813', '2000/01/01', 'Odessa'),
('Королева', 'Королевская', 'Королева', '2021/03/09', 'kol56@mail.ru', '+380930331814', '1998/06/01', 'Львов'),
('Лика', 'Козлова', 'Петровна', '2020/04/09', 'уeva93@mail.ru', '+380930331815', '1968/07/01', 'Харьков'),
('Буш', 'Бушко', 'Петрович', '2020/11/09', 'vlamis9@mail.ru', '+380930331816', '1970/08/01', 'Kiev'),
('Джон', 'Дональд', 'Трамп', '2020/10/29', 'abust@mail.ru', '+380930331817', '1980/09/01', 'Kiev'),
('Владимир', 'Путин', 'Владимирович', '2020/11/19', 'dota2@mail.ru', '+380930331818', '1990/11/01', 'Odessa'),
('Вася', 'Пупкин', 'Васильев', '2020/12/17', 'dora195@mail.ru', '+380930331819', '1996/11/01', 'Odessa'),
('Степан', 'Степанов', 'Степаненко', '2020/12/28', 'ks16@mail.ru', '+380930331820', '1997/12/01', 'Львов'),
('Царевна2', 'Цакова', 'Империя2', '2020/09/06', 'line9ka@mail.ru', '+380930331821', '1968/04/01', 'Харьков')
INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomPhone, Birthday, CustomMail)
values ('Билли', 'Айлиш', 'Alensi', '2020/05/22', '+380930331873', '1984/05/17', 'gromov88@mail.ru'),
('Бен', 'Сталин','Keni', '2020/06/29', '+380930731822', '1988/04/17', 'serb6@mail.ru'),
('Alan', 'Baduev', 'Klan',  '2020/04/24', '+380939331824', '1987/09/17', 'inte1@mail.ru')
INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomMail, CustomPhone)
values ('Sesil', 'Senli', 'Diablosa', '2020/08/11', 'gromovo9@i.ua', '+380937331824'),
('Leina', 'Karlo', 'Serma', '2020/12/30', 'sladkoe@i.ua', '+380939336824')
INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomMail, CustomPhone)
values ('Serb', 'Kasha', 'Diablosa', '2020/09/11', 'candy@i.ua', '+380937331854'),
('Lilis', 'Linko', 'Петровна', '2020/05/20', 'sweet@i.ua', '+380939736824'),
('Serb', 'Kasha', 'Olegovna', '2020/09/11', 'swan@i.ua', '+380934731854'),
('Лола', 'Козлова', 'Трамп', '2020/07/20', 'crystal@i.ua', '+380939666824'),
('Аврора', 'Kорниленко', 'Васильев', '2020/11/11', 'flower@i.ua', '+380938531854'),
('Лика', 'Панчук', 'Olegovna', '2020/06/20', 'h0pe@i.ua', '+380939736835'),
('Миранда', 'Пилипенко', 'Diablosa', '2020/09/11', 'Iphone12@i.ua', '+380937331834'),
('Клаус', 'Свердлов', 'Vladimirov', '2020/05/20', 'foreverg@i.ua', '+380939736832'),
('Степан', 'Маркс', 'Olegovna', '2020/09/11', 'LoveDen@i.ua', '+380937331867'),
('Пабло', 'Медиана', 'Святослав', '2020/05/20', 'LiveDnipro@i.ua', '+380939736829'),
('Наташа', 'Кофтенко', 'Васильева', '2020/10/11', 'house2020@i.ua', '+380937331858'),
('Lilis', 'Виноградов', 'Петровна', '2020/11/26', 'mause3000@i.ua', '+380939736827'),
('Мила', 'Кася', 'Dia', '2020/09/24', 'Launch26@i.ua', '+380937331856'),
('Света', 'Linko', 'Vladimirovna', '2020/05/21', 'Messages2time@i.ua', '+380939736825')
INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomMail, CustomPhone,Birthday)
values ('Olga', 'Pass', 'Vadimovna', '2020/04/25', 'dollar@i.ua', '+380837331845','2020/09/25')
INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomPhone, Birthday)
values ('Вишнев', 'Торт', 'Своя линия', '2020/04/26', '+380537331845','2020/09/26')
INSERT INTO Customers (CustomName, CustomSurname, CustomMiddle, BeginDate, CustomPhone, Birthday)
values ('Киевский', 'Торт', 'Своя линия', '2020/04/13', '+380547331845','2020/03/26')



INSERT INTO currency (name, code, symbol) VALUES ('Leke', 'ALL', 'Lek');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'USD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Afghanis', 'AFN', '؋');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'ARS', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Guilders', 'AWG', 'ƒ');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'AUD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('New Manats', 'AZN', 'ман');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'BSD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'BBD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Rubles', 'BYR', 'p.');
INSERT INTO currency (name, code, symbol) VALUES ('Euro', 'EUR', '€');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'BZD', 'BZ$');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'BMD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Bolivianos', 'BOB', '$b');
INSERT INTO currency (name, code, symbol) VALUES ('Convertible Marka', 'BAM', 'KM');
INSERT INTO currency (name, code, symbol) VALUES ('Pula', 'BWP', 'P');
INSERT INTO currency (name, code, symbol) VALUES ('Leva', 'BGN', 'лв');
INSERT INTO currency (name, code, symbol) VALUES ('Reais', 'BRL', 'R$');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'GBP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'BND', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Riels', 'KHR', '៛');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'CAD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'KYD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'CLP', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Yuan Renminbi', 'CNY', '¥');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'COP', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Colón', 'CRC', '₡');
INSERT INTO currency (name, code, symbol) VALUES ('Kuna', 'HRK', 'kn');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'CUP', '₱');
INSERT INTO currency (name, code, symbol) VALUES ('Koruny', 'CZK', 'Kč');
INSERT INTO currency (name, code, symbol) VALUES ('Kroner', 'DKK', 'kr');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'DOP ', 'RD$');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'XCD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'EGP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Colones', 'SVC', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'FKP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'FJD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Cedis', 'GHC', '¢');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'GIP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Quetzales', 'GTQ', 'Q');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'GGP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'GYD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Lempiras', 'HNL', 'L');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'HKD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Forint', 'HUF', 'Ft');
INSERT INTO currency (name, code, symbol) VALUES ('Kronur', 'ISK', 'kr');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'INR', 'Rp');
INSERT INTO currency (name, code, symbol) VALUES ('Rupiahs', 'IDR', 'Rp');
INSERT INTO currency (name, code, symbol) VALUES ('Rials', 'IRR', '﷼');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'IMP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('New Shekels', 'ILS', '₪');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'JMD', 'J$');
INSERT INTO currency (name, code, symbol) VALUES ('Yen', 'JPY', '¥');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'JEP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Tenge', 'KZT', 'лв');
INSERT INTO currency (name, code, symbol) VALUES ('Won', 'KPW', '₩');
INSERT INTO currency (name, code, symbol) VALUES ('Won', 'KRW', '₩');
INSERT INTO currency (name, code, symbol) VALUES ('Soms', 'KGS', 'лв');
INSERT INTO currency (name, code, symbol) VALUES ('Kips', 'LAK', '₭');
INSERT INTO currency (name, code, symbol) VALUES ('Lati', 'LVL', 'Ls');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'LBP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'LRD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Switzerland Francs', 'CHF', 'CHF');
INSERT INTO currency (name, code, symbol) VALUES ('Litai', 'LTL', 'Lt');
INSERT INTO currency (name, code, symbol) VALUES ('Denars', 'MKD', 'ден');
INSERT INTO currency (name, code, symbol) VALUES ('Ringgits', 'MYR', 'RM');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'MUR', '₨');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'MXN', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Tugriks', 'MNT', '₮');
INSERT INTO currency (name, code, symbol) VALUES ('Meticais', 'MZN', 'MT');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'NAD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'NPR', '₨');
INSERT INTO currency (name, code, symbol) VALUES ('Guilders', 'ANG', 'ƒ');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'NZD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Cordobas', 'NIO', 'C$');
INSERT INTO currency (name, code, symbol) VALUES ('Nairas', 'NGN', '₦');
INSERT INTO currency (name, code, symbol) VALUES ('Krone', 'NOK', 'kr');
INSERT INTO currency (name, code, symbol) VALUES ('Rials', 'OMR', '﷼');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'PKR', '₨');
INSERT INTO currency (name, code, symbol) VALUES ('Balboa', 'PAB', 'B/.');
INSERT INTO currency (name, code, symbol) VALUES ('Guarani', 'PYG', 'Gs');
INSERT INTO currency (name, code, symbol) VALUES ('Nuevos Soles', 'PEN', 'S/.');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'PHP', 'Php');
INSERT INTO currency (name, code, symbol) VALUES ('Zlotych', 'PLN', 'zł');
INSERT INTO currency (name, code, symbol) VALUES ('Rials', 'QAR', '﷼');
INSERT INTO currency (name, code, symbol) VALUES ('New Lei', 'RON', 'lei');
INSERT INTO currency (name, code, symbol) VALUES ('Rubles', 'RUB', 'руб');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'SHP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Riyals', 'SAR', '﷼');
INSERT INTO currency (name, code, symbol) VALUES ('Dinars', 'RSD', 'Дин.');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'SCR', '₨');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'SGD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'SBD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Shillings', 'SOS', 'S');
INSERT INTO currency (name, code, symbol) VALUES ('Rand', 'ZAR', 'R');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'LKR', '₨');
INSERT INTO currency (name, code, symbol) VALUES ('Kronor', 'SEK', 'kr');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'SRD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Pounds', 'SYP', '£');
INSERT INTO currency (name, code, symbol) VALUES ('New Dollars', 'TWD', 'NT$');
INSERT INTO currency (name, code, symbol) VALUES ('Baht', 'THB', '฿');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'TTD', 'TT$');
INSERT INTO currency (name, code, symbol) VALUES ('Lira', 'TRY', '₺');
INSERT INTO currency (name, code, symbol) VALUES ('Liras', 'TRL', '£');
INSERT INTO currency (name, code, symbol) VALUES ('Dollars', 'TVD', '$');
INSERT INTO currency (name, code, symbol) VALUES ('Hryvnia', 'UAH', '₴');
INSERT INTO currency (name, code, symbol) VALUES ('Pesos', 'UYU', '$U');
INSERT INTO currency (name, code, symbol) VALUES ('Sums', 'UZS', 'лв');
INSERT INTO currency (name, code, symbol) VALUES ('Bolivares Fuertes', 'VEF', 'Bs');
INSERT INTO currency (name, code, symbol) VALUES ('Dong', 'VND', '₫');
INSERT INTO currency (name, code, symbol) VALUES ('Rials', 'YER', '﷼');
INSERT INTO currency (name, code, symbol) VALUES ('Zimbabwe Dollars', 'ZWD', 'Z$');
INSERT INTO currency (name, code, symbol) VALUES ('Rupees', 'INR', '₹');