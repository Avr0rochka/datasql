--1
Drop Database if Exists DBSuperMarket;

--2
--done

--3
IF OBJECT_ID (N'Products', N'U') IS NOT NULL 
   SELECT 1 AS res ELSE SELECT 0 AS res;

--4
--see Infrostaction.sql

--5
--see InsertsData.sql

--6
--ok

--7
Select CustomSurname, CustomName, CustomMiddle, CustomMail, CustomPhone
From Customers
Where MONTH(Birthday) = 11 and DAY(Birthday) = 1

--8
Select (CustomSurname  + SUBSTRING(CustomName, 1, 1) + '.' + SUBSTRING(CustomMiddle, 1, 1) + '.') AS Name , BeginDate, Town
From Customers
Where YEAR(BeginDate) = 2020 AND (MONTH(BeginDate) BETWEEN 1 AND 6) 

--9
Select ProdName, ProdDate, Country, Currency
From Products
ORDER BY Country

--10
Select BeginDate, Town, CustomSurname, CustomName, CustomMiddle, CustomMail, CustomPhone, Birthday
From Customers
Where Birthday is NULL OR Town is NULL
ORDER BY CustomSurname, CustomName, CustomMiddle

--11
Select InvDate,Sum_nt,Sum_wt
From Invoice
Where YEAR(InvDate) = 2020 AND MONTH(InvDate) = 9
ORDER BY InvDate DESC

--12
Select CustomSurname + ' ' + CustomName + ' ' + CustomMiddle AS Name, I.Sum_wt
From Customers AS C Join Invoice AS I ON I.CustomID = C.CustomID
Where C.Town = 'Kiev' OR C.Town = 'Киев'

--13
Select Town, count(CustomSurname) As Name
From Customers
Group by Town
Order by Name Desc

--14
Select ProdName, Article, ProdDate
From Products
Where YEAR(ProdDate) = 2020 AND MONTH(ProdDate) = 7
Order by Article, ProdDate, ProdName

--15
Select CustomSurname + ' ' + CustomName + ' ' + CustomMiddle AS Name
From Customers AS C Join Invoice AS I ON I.CustomID = C.CustomID
Where InvDate = '2020-07-16'

--16
UPDATE Products
Set ProdPrice = ProdPrice - (ProdPrice * 0.02)
From 
(Select ProdID From InvoiceDetail AS ID join Invoice AS I ON I.InvID = ID.InvID Where InvDate = '2020-09-11') AS ID
Where ID.ProdID = Products.ProdID AND Products.ProdID IN (7, 13, 45, 10, 27, 33)

--17
UPDATE Products 
Set Color = 'Зелёный'
From
(Select ProdID, SUBSTRING(Article, 3, 6) AS Number From Products Where SUBSTRING(Article, 3, 6) Between  138000 AND 150000) AS P
Where P.ProdID = Products.ProdID

--18
INSERT INTO Products (ProdName, Article, Color, ProdDate, Country, ProdPrice)
values 
('Носки', 'XX100001', 'Серый', '2020/06/01', 'Украина', 5),
('Носки', 'XX100002', 'Коричневый', '2020/06/01', 'Украина', 5),
('Носки', 'XX100003', 'Черный', '2020/06/01', 'Украина', 5),
('Носки', 'XX100004', 'Красный', '2020/06/01', 'Украина', 5),
('Носки', 'XX100005', 'Красный', '2020/06/01', 'Украина', 5)

--19
DELETE Products
FROM
(
	Select P.ProdID
	From Products AS P 
	LEFT JOIN InvoiceDetail AS ID ON P.ProdID=ID.ProdID
	Where ID.ProdID IS NULL
) AS PID
Where PID.ProdID = Products.ProdID AND (Products.Color = 'Red' OR Products.Color = 'Красный')

--20
Select CustomSurname + ' ' + CustomName + ' ' + CustomMiddle AS Name 
From Customers AS C 
join Discount AS D ON C.CustomID=D.CustomID
Where C.Town in ('Dnepr', 'Zaporizhzhia', 'Запорожье', 'Днепр') AND D.DiscPercent = 20

--21
Select P.ProdName, ID.Quant, Town
From Products AS P 
join InvoiceDetail AS ID ON P.ProdID = ID.ProdID
join Invoice AS I ON I.InvID = ID.InvID
join Customers AS C ON C.CustomID = I.CustomID
Where Town = 'Odessa' OR Town = 'Одесса'
ORDER BY ID.Quant DESC

--22
Select ProdName, Country, Currency, C.name
From Products AS P join currency AS C ON C.code = P.Currency
Where Country not in ('Ukraine', 'Украина')

--23
Select * From [GetInfoCustomers]

--24
Select Sum(Sum_nt) , Sum(Tax), Sum(Sum_wt), 'Winter' AS Season
From Invoice
Where MONTH(InvDate) in (12,1,2)
UNION ALL
Select Sum(Sum_nt) , Sum(Tax), Sum(Sum_wt), 'Spring'
From Invoice
Where MONTH(InvDate) in (3,4,5)
UNION ALL
Select Sum(Sum_nt) , Sum(Tax), Sum(Sum_wt), 'Summer'
From Invoice
Where MONTH(InvDate) in (6,7,8)
UNION ALL
Select Sum(Sum_nt) , Sum(Tax), Sum(Sum_wt), 'Autumn'
From Invoice
Where MONTH(InvDate) in (9,10,11)

--25
exec [dbo].[DiscountInsert]

--26
SELECT * from [dbo].[GetTown]('Валерьянка') 

--27
exec  GetDiscount

--28
SELECT * from dbo.[GetProductsByCountry]('Украина') 