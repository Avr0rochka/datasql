--23
--Select * From [GetInfoCustomers]

CREATE VIEW [dbo].[GetInfoCustomers]
AS
Select CustomSurname + ' ' + CustomName + ' ' + CustomMiddle AS Name, BeginDate, DiscDate, DiscPercent, DiscSumma, ProdName, Quant --23
From Customers AS C
	left join Discount AS D ON C.CustomID=D.CustomID
	left join Invoice AS I ON I.CustomID=C.CustomID
	left join InvoiceDetail AS ID ON ID.InvID=I.InvID
	left join Products AS P  ON P.ProdID=ID.ProdID

--25
--exec [dbo].[DiscountInsert]

CREATE FUNCTION [dbo].[GetDiscSumma]
(
	@CustomID int
)
RETURNS float
AS 
	BEGIN
	RETURN 
	(
		ISNULL((Select Sum(I.Sum_wt * 0.05)
		From Invoice AS I
		Where I.CustomID = @CustomID ), 0)
		
	)
	
END

CREATE FUNCTION [dbo].[GetDiscPercent]
(
	@BeginDate smalldatetime
)
RETURNS int
AS 
	BEGIN
	RETURN 
	(
		CASE 
		WHEN (DATEDIFF(month, @BeginDate, GETUTCDATE()) < 3) THEN 5 
		WHEN (DATEDIFF(month, @BeginDate, GETUTCDATE()) BETWEEN 3 AND 6 )THEN 10 
		WHEN (DATEDIFF(month, @BeginDate, GETUTCDATE()) BETWEEN 7 AND 9 )THEN 15 
		WHEN (DATEDIFF(month, @BeginDate, GETUTCDATE()) > 9 )THEN 20 
		END
	)
END

CREATE FUNCTION [dbo].[GetDiscDate]
(
	@BeginDate smalldatetime
)
RETURNS smalldatetime
AS 
	BEGIN
	RETURN dateadd(day, 1, @BeginDate)
END

--Поле InvID в таблице Дисконт не стала заполнять. 
--Посколько данный идентификатор является уникальным для каждой накладной. 
--При налиичии более одной накладной на клиента, не ясен смысл связывать таблицы по данному полю.


CREATE PROCEDURE [dbo].[DiscountInsert]
AS 
BEGIN

create table #DiscPeople
(CustomID int, DiscDate datetime, DiscPercent int, DiscSumma decimal)

insert into #DiscPeople
Select C.CustomID, [dbo].[GetDiscDate] (C.BeginDate) as DiscDate, [dbo].[GetDiscPercent] (C.BeginDate) as DiscPercent, [dbo].[GetDiscSumma](C.CustomID) as DiscSumma
	from Customers as C
	left join Discount AS D ON D.CustomID = C.CustomID
	Where D.CustomID IS NULL
	Order by C.CustomID

	INSERT INTO dbo.Discount (CustomID, DiscDate, DiscPercent, DiscSumma)
	Select * from #DiscPeople	
END

CREATE PROCEDURE [dbo].[IsCorrectCustomer]
(
@CustomID int
)
AS 
BEGIN
	DECLARE @Result bit = 0;		
	IF EXISTS (
	select top(1)*
	From Customers
	Where CustomID = @CustomID and Birthday IS NOT NULL and Town IS NOT NULL and CustomMail IS NOT NULL
	)
	begin 
		set @Result = 1
	end
	
	SELECT @Result	
END

--26
--SELECT * from [dbo].[GetTown]('Валерьянка') 

CREATE FUNCTION [dbo].[GetTown]
(
	@ProductName varchar(300)
)
RETURNS TABLE  
AS  
RETURN

	Select C.Town
	From Products AS P 
	join InvoiceDetail AS ID ON P.ProdID = ID.ProdID
	join Invoice AS I ON I.InvID = ID.InvID
	join Customers AS C ON C.CustomID = I.CustomID
	Where P.ProdName = @ProductName;

--27
--exec  GetDiscount

CREATE PROCEDURE [dbo].[GetDiscount]
AS
BEGIN
create table #bdays
(CustomID int, BeginDate datetime, BeginYear int, BeginMonth int)

insert into #bdays
Select C.CustomID, C.BeginDate, YEAR(C.BeginDate), MONTH(C.BeginDate)
	From Customers AS C 
	left join Discount AS D ON C.CustomID=D.CustomID
	Where DATEPART(d, Birthday) = DATEPART(d, GETUTCDATE())
	AND DATEPART(m, Birthday) = DATEPART(m, GETUTCDATE())

		
	UPDATE Discount
	SET DiscSumma = CASE 
						When BeginYear = 2020 AND BeginMonth BETWEEN 7 AND 12 THEN DiscSumma + 500
						When BeginYear = 2020 AND BeginMonth BETWEEN 1 AND 6 THEN DiscSumma + 1000
					END
	From 
	(Select * From #bdays) AS b2
	Where b2.CustomID = Discount.CustomID
END;

--28
--SELECT * from dbo.[GetProductsByCountry]('Украина') 

CREATE FUNCTION [dbo].[GetProductsByCountry]
(
  @Country nvarchar(300)
)
RETURNS TABLE  
AS  
RETURN
(
Select ProdName
From Products
Where Country = @Country
);